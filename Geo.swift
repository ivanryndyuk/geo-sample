//
//  Geo.swift
//  Pilot Expert
//
//  Created by Ivan Ryndyuk on 3.10.2017.
//  Copyright © 2017 Map Expert. All rights reserved.
//

import UIKit

class Geo: NSObject {
    
    //MARK: - VARS
    
    let r: Double = 6371000 // Earth radius in meters.
    
    
    
    //MARK: - FUNCS
    
    func equals(_ coor1: Position, _ coor2: Position) -> Bool {
        return coor1.latitude == coor2.latitude && coor1.longitude == coor2.longitude
    }
    
    func distance(_ point1: Position, _ point2: Position) -> Double {
        let a = acos(sin(point1.latitude.toRadians()) * sin(point2.latitude.toRadians()) + cos(point1.latitude.toRadians()) * cos(point2.latitude.toRadians()) * cos(point1.longitude.toRadians() - point2.longitude.toRadians()))
        return a * r
    }
    
    func pathLength(coors: [Position]) -> Double {
        var length: Double = 0
        
        for i in 0..<(coors.count - 1) {
            length += distance(coors[i], coors[i + 1])
        }
        
        return length
    }
    
    func heightFrom(gsd: Double, sw: Double, fr: Double, swpx: Double) -> Double {
        return (gsd * fr * swpx) / sw
    }
    
    func gsdFrom(h: Double, sw: Double, fr: Double, swpx: Double) -> Double {
        return (sw * h) / (fr * swpx)
    }
    
    func longitudialBasis(swpx: Double, ox: Double, gsd: Double) -> Double {
        //temp
        return swpx * (1 - ox) * gsd
    }
    
    func transverseBasis(shpx: Double, oy: Double, gsd: Double) -> Double {
        return shpx * (1 - oy) * gsd
        
    }
    
    func move(coor: Position, delta: Double, az: Double) -> Position {
        let offsetRads = delta / r
        let azRads = az.toRadians()
        let latRads = coor.latitude.toRadians()
        let lonRads = coor.longitude.toRadians()
        
        let newLatRads = asin(sin(latRads) * cos(offsetRads) + cos(latRads) * sin(offsetRads) * cos(azRads))
        let newLonRads = lonRads + atan2(sin(azRads) * sin(offsetRads) * cos(latRads), cos(offsetRads) - sin(latRads) * sin(newLatRads))
        
        return Position.make(newLatRads.toDegrees(), newLonRads.toDegrees())
    }
    
    
    /**
     Get shot coordinates for area flight.
     - Parameter areaCoors: Initial coors of flight area.
     - Parameter ox: Longitudinal overlap.
     - Parameter oy: Transverse overlap.
     - Parameter camera: Camera settings object.
     - Parameter gsd: Ground sample distance.
     - Parameter h: Height over ground.
     - Returns: List of coordinates [Position].
    */
    
    func getAreaShotCoors(areaCoors: [Position], ox: Double = 0.7, oy: Double = 0.3, camera: Camera = .standard, gsd: Double? = nil, h: Double? = nil) -> [Position] {
        var result = [Position]()
        var groundSampleDistance: Double
        
        if let gsd = gsd {
            groundSampleDistance = gsd
        } else if let h = h {
            groundSampleDistance = gsdFrom(h: h, sw: camera.sw, fr: camera.frl, swpx: camera.swpx)
        } else {
            return result
        }
        
        // Longitudial basis (distance between shots)
        let bx = longitudialBasis(swpx: camera.swpx, ox: ox, gsd: groundSampleDistance)
        
        // Transverse basis (distance between paths)
        let by = transverseBasis(shpx: camera.shpx, oy: oy , gsd: groundSampleDistance)
        
        let containingAreaCoors = generateContainingAreaCoors(coors: areaCoors, bx: bx, by: by)
        let containingAreaLength = distance(containingAreaCoors[0], containingAreaCoors[1])
        let containingAreaWidth = distance(containingAreaCoors[1], containingAreaCoors[2])
        let pathCount = Int(ceil(containingAreaWidth / by))
        let shotCount = Int(ceil(containingAreaLength / bx))
        
        for p in 0..<pathCount {
            let lon = move(coor: containingAreaCoors[0], delta: Double(p) * by, az: 90).longitude
            let intersectionsTouple = intersectionPoints(
                a1: Position.make(containingAreaCoors[0].latitude, lon),
                a2: Position.make(containingAreaCoors[1].latitude, lon),
                vertexs: areaCoors,
                z: containingAreaCoors[0])
            
            for s in 0..<shotCount {
                var factor = s
                
                if !p.isEven() {
                    factor = shotCount - s - 1
                }
                
                let lat = move(coor: containingAreaCoors[0], delta: Double(factor) * bx, az: 0).latitude
                let intersections = intersectionsTouple.0
                
                if intersections.count == 2 && s == 0 {
                    if p.isEven() {
                        if intersections[0].latitude <= intersections[1].latitude {
                            addIntersectionIfNeeded(intersections[0], bx: bx, result: &result)
                        } else {
                            addIntersectionIfNeeded(intersections[1], bx: bx, result: &result)
                        }
                    } else {
                        if intersections[0].latitude <= intersections[1].latitude {
                            addIntersectionIfNeeded(intersections[1], bx: bx, result: &result)
                        } else {
                            addIntersectionIfNeeded(intersections[0], bx: bx, result: &result)
                        }
                    }
                }
                
                if intersections.count == 0 {
                    continue
                } else if intersections.count == 1 {
                    let c = Position.make(lat, lon)
                    if equals(c, intersections[0]) {
                        addIntersectionIfNeeded(c, bx: bx, result: &result, isFirst: true)
                    }
                } else {
                    let c = Position.make(lat, lon)
                    let low = intersectionsTouple.1
                    let high = intersectionsTouple.2
                    
                    if c.latitude < high.latitude && c.latitude > low.latitude {
                        addIntersectionIfNeeded(c, bx: bx, result: &result, isFirst: true)
                    }
                }
                
                if intersections.count == 2 && s == (shotCount - 1) {
                    if p.isEven() {
                        if intersections[0].latitude <= intersections[1].latitude {
                            addIntersectionIfNeeded(intersections[1], bx: bx, result: &result)
                        } else {
                            addIntersectionIfNeeded(intersections[0], bx: bx, result: &result)
                        }
                    } else {
                        if intersections[0].latitude <= intersections[1].latitude {
                            addIntersectionIfNeeded(intersections[0], bx: bx, result: &result)
                        } else {
                            addIntersectionIfNeeded(intersections[1], bx: bx, result: &result)
                        }
                    }
                }
            }
        }
        
        return result
    }
    
    func addIntersectionIfNeeded(_ intersection: Position, bx: Double, result: inout [Position], isFirst: Bool = false) {
        if let previous = result.last {
            if distance(previous, intersection) >= bx / 2.0 {
                result.append(intersection)
            } else if isFirst {
                result.removeLast()
                result.append(intersection)
            }
        } else {
            result.append(intersection)
        }
    }
    
    
    /** Generates coordinates of new area, containinf specified coors. */
    
    func generateContainingAreaCoors(coors: [Position], bx: Double, by: Double) -> [Position] {
        var minLat: Double = 0
        var maxLat: Double = 0
        var minLon: Double = 0
        var maxLon: Double = 0
        
        for (i, coor) in coors.enumerated() {
            if i == 0 {
                minLat = coor.latitude
                maxLat = coor.latitude
                minLon = coor.longitude
                maxLon = coor.longitude
            } else {
                minLat = min(minLat, coor.latitude)
                maxLat = max(maxLat, coor.latitude)
                minLon = min(minLon, coor.longitude)
                maxLon = max(maxLon, coor.longitude)
            }
        }
        
        var p1 = Position.make(minLat, minLon)
        p1 = move(coor: p1, delta: bx / 2, az: 180)
        p1 = move(coor: p1, delta: by / 2, az: 270)
        
        var p2 = Position.make(maxLat, minLon)
        p2 = move(coor: p2, delta: bx / 2, az: 0)
        p2 = move(coor: p2, delta: by / 2, az: 270)
        
        var p3 = Position.make(maxLat, maxLon)
        p3 = move(coor: p3, delta: bx / 2, az: 0)
        p3 = move(coor: p3, delta: by / 2, az: 90)
        
        var p4 = Position.make(minLat, maxLon)
        p4 = move(coor: p4, delta: bx / 2, az: 180)
        p4 = move(coor: p4, delta: by / 2, az: 90)
        
        return [p1, p2, p3, p4]
    }
    
    
    func getOneFlightPositions(with remainedPositions: [Position], remainingBattery: UInt, from location: Position? = nil) -> [Position] {
        var positions = remainedPositions
        
        if let location = location {
            positions.insert(location, at: 0)
        }
        
        var result = [Position]()
        var previousLocation = positions[0]
        var remainedTime = Int(remainingBattery) / 100 * DJI.batteryTime
        
        for i in 1..<positions.count {
            let currentLocation = positions[i]
            let ds = distance(currentLocation, previousLocation)
            let dt = Int(ds) * DJI.velocity
            
            if remainedTime - dt >= 0 {
                result.append(previousLocation)
                previousLocation = currentLocation
                remainedTime -= dt
            } else {
                break
            }
        }
        
        return result
    }
    
    func intersectionPoints(a1: Position, a2: Position, vertexs: [Position], z: Position) -> ([Position], Position, Position) {
        var result = [Position]()
        var lonMin: Double = 0
        var lonMax: Double = 0
        var latMin: Double = 0
        var latMax: Double = 0
        
        let a = LineSegment(begin: a1, end: a2, zero: z)
        
        for i in 0..<vertexs.count {
            let v1: Position
            let v2: Position
            
            if i == (vertexs.count - 1) {
                v1 = vertexs[i]
                v2 = vertexs[0]
            } else {
                v1 = vertexs[i]
                v2 = vertexs[i + 1]
            }
            
            let v = LineSegment(begin: v1, end: v2, zero: z)
            
            if a.a * v.b != v.a * a.b {
                // If a and b aren't parallel we solve a system of line equations to find intersection point.
                
                let x = ((a.b * v.c) - (a.c * v.b)) / ((a.a * v.b) - (a.b * v.a))
                let y = (-1) * ((v.a * x) + v.c) / v.b
                
                let belongsAx = a.begin.x <= x && x <= a.end.x || a.end.x <= x && x <= a.begin.x
                let belongsAy = a.begin.y <= y && y <= a.end.y || a.end.y <= y && y <= a.begin.y
                let belongsVx = v.begin.x <= x && x <= v.end.x || v.end.x <= x && x <= v.begin.x
                let belongsVy = v.begin.y <= y && y <= v.end.y || v.end.y <= y && y <= v.begin.y
                
                if (belongsAx && belongsVy) || (belongsVx && belongsAy) {
                    if result.count == 0 {
                        latMin = z.latitude + x
                        latMax = z.latitude + x
                        lonMin = z.longitude + y
                        lonMax = z.longitude + y
                    } else {
                        latMin = min(latMin, z.latitude + x)
                        latMax = max(latMax, z.latitude + x)
                        lonMin = min(lonMin, z.longitude + y)
                        lonMax = max(lonMax, z.longitude + y)
                    }
                    
                    result.append(Position.make(z.latitude + x, z.longitude + y))
                }
            }
        }
        
        return (result, Position.make(latMin, lonMin), Position.make(latMax, lonMax))
    }
    
    func getCircleShapeCoors(center: Position, radius: Double, stepAngle: Int = 8) -> [Position] {
        let numberOfPoints: Int = 360 / stepAngle
        let distanceRadians: Double = radius / r
        let centerLatitudeRadians: Double = center.latitude * .pi / 180.0
        let centerLongitudeRadians: Double = center.longitude * .pi / 180.0
        
        var coors = [Position]()
        
        for i in 0..<numberOfPoints {
            let degrees: Double = Double(i * stepAngle)
            let degreesRadians: Double = degrees * .pi / 180.0
            let pointLatitudeRadians = asin(sin(centerLatitudeRadians) * cos(distanceRadians) + cos(centerLatitudeRadians) * sin(distanceRadians) * cos(degreesRadians))
            let pointLongitudeRadians = centerLongitudeRadians + atan2(sin(degreesRadians) * sin(distanceRadians) * cos(centerLatitudeRadians), cos(distanceRadians) - sin(centerLatitudeRadians) * sin(pointLatitudeRadians))
            let pointLatitude = pointLatitudeRadians * 180 / .pi
            let pointLongitude = pointLongitudeRadians * 180 / .pi
            coors.append(Position(latitude: pointLatitude, longitude: pointLongitude))
        }
        
        return coors
    }
    
}
